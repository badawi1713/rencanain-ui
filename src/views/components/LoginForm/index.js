import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Modal, Button, Card, Form } from "react-bootstrap";
import { motion } from "framer-motion";
const LoginForm = ({ show, handleClose }) => {
  const [forgotPasswordActive, setForgotPasswordActive] = useState(false);

  const horizontalLine = {
    flex: 1,
    height: "2px",
    backgroundColor: "#e7e7e7",
  };
  const iconStyle = {
    color: "#969595",
    cursor: "pointer",
  };

  const forgotPasswordHandler = (e) => {
    e.preventDefault();

    setForgotPasswordActive(!forgotPasswordActive);
  };

  const Login = () => (
    <>
      <h5 className="my-4">Masuk</h5>
      <Form>
        <Form.Group>
          <Form.Label>Email</Form.Label>
          <Form.Control type="email" required />
        </Form.Group>
        <Form.Group>
          <Form.Label>Password</Form.Label>
          <Form.Control type="email" required />
        </Form.Group>
        <div className="text-right mb-3">
          <Button onClick={forgotPasswordHandler} variant="link">
            Lupa Password?
          </Button>
        </div>
        <Button type="submit" block>
          Masuk
        </Button>
      </Form>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
        className="my-3"
      >
        <div style={horizontalLine} />
        <div className="px-3 font-weight-light">Belum memiliki akun?</div>
        <div style={horizontalLine} />
      </div>
      <Link to="/register">
        <Button
          type="button"
          className="btn-light btn-outline-primary mb-3"
          block
        >
          Daftar
        </Button>
      </Link>
    </>
  );

  const ForgotPassword = () => (
    <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }}>
      <h5 className="my-4">Lupa Password?</h5>
      <p>Silakan isi email anda untuk mengubah password.</p>
      <Form>
        <Form.Group>
          <Form.Label>Email</Form.Label>
          <Form.Control type="email" required />
        </Form.Group>
        <div className="text-right mb-3">
          <Button onClick={forgotPasswordHandler} variant="link">
            Kembali ke Login
          </Button>
        </div>
        <Button type="submit" block>
          Kirim
        </Button>
      </Form>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
        className="my-3"
      >
        <div style={horizontalLine} />
        <div className="px-3 font-weight-light">Belum memiliki akun?</div>
        <div style={horizontalLine} />
      </div>
      <Link to="/register">
        <Button
          type="button"
          className="btn-light btn-outline-primary mb-3"
          block
        >
          Daftar
        </Button>
      </Link>
    </motion.div>
  );
  return (
    <Modal
      aria-labelledby="contained-modal-title-vcenter"
      centered
      show={show}
      onHide={handleClose}
    >
      <Card
        style={{
          boxShadow: "0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)",
        }}
      >
        <Card.Body>
          <div onClick={handleClose} className="text-right">
            <motion.i
              whileHover={{ color: "#202020" }}
              style={iconStyle}
              className="fas fa-times"
            ></motion.i>
          </div>
          {!forgotPasswordActive ? <Login /> : <ForgotPassword />}
        </Card.Body>
      </Card>
    </Modal>
  );
};

export default LoginForm;
